<div class="">
    <h1 class="h3 mb-4 text-gray-800">Detail Client</h1>

    <!-- Page Heading -->

    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">A propos</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Encours</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">Finalisée</a>
                        </li>
                    </ul>
                </div>


                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                        <div class="card">
                            <div class="card-body">
                                <div class="mt-40">

                                    <div class="animated fadeInDown shadow px-1">
                                        <div class="text-center p-1">
                                            <div>
                                                <i class="fa fa-calendar-check"></i> <strong><u>Date inscription:</u></strong>
                                                <span class="text-muted">12/04/2020</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div  class="card-body row animated fadeInUp">
                                        <div class=" col-md-4 col-sm-12 mt-10">
                                            <div class="border-danger">
                                                <h6><span class="fa fa-user text-dark"></span> Nom Complet :<strong><u></u> Moussa Danfakha</u></strong></h6>

                                            </div>

                                        </div>
                                        <div class=" col-md-4 col-sm-12 mt-10 mb-2">
                                            <div class="border-danger">
                                                <h6 > <span class="fa fa-flag text-dark"></span> Pays d'origine :<strong><u></u>Sénégal</u></strong></h6>
                                            </div>

                                        </div>
                                        <div class=" col-md-4 col-sm-12 mt-10">
                                            <div class="border-danger">
                                                <h6 ><span class="fa fa-home text-dark"></span> Adresse actuel :<strong><u></u>PARIS </u></strong></h6>

                                            </div>

                                        </div>
                                        <div class=" col-md-4 col-sm-12 mt-10">
                                            <div class="border-danger">
                                                <h6 ><span class="fa fa-phone text-dark"></span> Numero tel :<strong><u></u> +33 001 34 34 56 67</u></strong></h6>

                                            </div>

                                        </div>
                                        <div class=" col-md-4 col-sm-12 mt-10">
                                            <div class="border-danger">
                                                <h6 > <span class="fa fa-envelope text-dark"></span> Email :<strong><u></u>Sénégal</u></strong></h6>
                                            </div>

                                        </div>
                                        <div class=" col-md-4 col-sm-12 mt-10">
                                            <div class="border-danger">
                                                <h6 > <span class="fa fa-phone text-dark"></span> Adresse actuel :<strong><u></u>PARIS </u></strong></h6>

                                            </div>

                                        </div>

                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                        <div class="card">
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                        <thead>
                                        <tr>
                                            <th>Code</th>
                                            <th>Date</th>
                                            <th>Pays</th>
                                            <th>Adresse</th>
                                            <th>Superficie</th>
                                            <th>Actions</th>
                                        </tr>
                                        </thead>

                                        <tbody>
                                        <tr>
                                            <td>Tiger Nixon</td>
                                            <td>System Architect</td>
                                            <td>Edinburgh</td>
                                            <td>61</td>
                                            <td>320,800</td>
                                            <td>
                                                <button href="detail-client.html" class="btn btn-primary btn-circle" data-toggle="modal" data-target=".bd-example-modal-lg">
                                                    <i class="fas fa-info-circle"></i>
                                                </button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Garrett Winters</td>
                                            <td>Accountant</td>
                                            <td>Tokyo</td>
                                            <td>63</td>
                                            <td>170,750</td>
                                            <td>
                                                <button href="detail-client.html" class="btn btn-primary btn-circle" data-toggle="modal" data-target=".bd-example-modal-lg">
                                                    <i class="fas fa-info-circle"></i>
                                                </button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Ashton Cox</td>
                                            <td>Junior Technical Author</td>
                                            <td>San Francisco</td>
                                            <td>66</td>
                                            <td>86,000</td>
                                            <td>
                                                <button href="detail-client.html" class="btn btn-primary btn-circle" data-toggle="modal" data-target=".bd-example-modal-lg">
                                                    <i class="fas fa-info-circle"></i>
                                                </button>
                                            </td>
                                        </tr>


                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
                        <div class="card">
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                        <thead>
                                        <tr>
                                            <th>Code</th>
                                            <th>Date</th>
                                            <th>Pays</th>
                                            <th>Adresse</th>
                                            <th>Superficie</th>
                                            <th>Actions</th>
                                        </tr>
                                        </thead>

                                        <tbody>
                                        <tr>
                                            <td>Tiger Nixon</td>
                                            <td>System Architect</td>
                                            <td>Edinburgh</td>
                                            <td>61</td>
                                            <td>320,800</td>
                                            <td>
                                                <button href="detail-client.html" class="btn btn-primary btn-circle" data-toggle="modal" data-target=".bd-example-modal-lg">
                                                    <i class="fas fa-info-circle"></i>
                                                </button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Garrett Winters</td>
                                            <td>Accountant</td>
                                            <td>Tokyo</td>
                                            <td>63</td>
                                            <td>170,750</td>
                                            <td>
                                                <button href="detail-client.html" class="btn btn-primary btn-circle" data-toggle="modal" data-target=".bd-example-modal-lg">
                                                    <i class="fas fa-info-circle"></i>
                                                </button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Ashton Cox</td>
                                            <td>Junior Technical Author</td>
                                            <td>San Francisco</td>
                                            <td>66</td>
                                            <td>86,000</td>
                                            <td>
                                                <button href="detail-client.html" class="btn btn-primary btn-circle" data-toggle="modal" data-target=".bd-example-modal-lg">
                                                    <i class="fas fa-info-circle"></i>
                                                </button>
                                            </td>
                                        </tr>


                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>
    <br>

</div>
